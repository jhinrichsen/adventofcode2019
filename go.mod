module gitlab.com/jhinrichsen/adventofcode2019

go 1.22

require (
	github.com/banyansecurity/golint-convert v0.0.2 // indirect
	github.com/boumenot/gocover-cobertura v1.2.0 // indirect
	golang.org/x/tools v0.0.0-20200526224456-8b020aee10d2 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)
